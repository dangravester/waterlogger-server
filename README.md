## Overview

The water logger server provides a web frontend for submitting logs and uses a PostgreSQL backend data store.
On startup, the server will create the database and logs table if they do not already exist.

## How to run using docker-compose

The `sample` directory contains the necessary configuration, which includes a password file for the database and a configuration file for the web server.
To deploy, run `docker-compose up -d` from the top-level directory.

### Accessing the service

The service may be accessed in a browser by going to localhost:8000
