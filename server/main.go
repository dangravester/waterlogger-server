package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"

	"bitbucket.org/dangravester/WaterLogger/controller"
	"bitbucket.org/dangravester/WaterLogger/data"
	"gopkg.in/yaml.v2"

	_ "github.com/lib/pq"
)

// command line arguments
var configFileName = flag.String("config", "service-config.yml", "Specify the configuration file to use.")
var verbose = flag.Bool("verbose", false, "Print additional runtime information to console.")

// main service configuration
type serviceConfig struct {
	Server struct {
		Port int `yaml:"port"`
	} `yaml:"server"`
	Database data.DBConfig `yaml:"database"`
}

func readConfiguration(fname string) (serviceConfig, error) {
	var config serviceConfig
	f, err := os.Open(fname)

	if err == nil {
		d := yaml.NewDecoder(f)
		err = d.Decode(&config)
	}

	return config, err
}

func main() {
	flag.Parse()

	vPrintf := func(f string, a ...interface{}) {
		if *verbose {
			fmt.Printf(f, a...)
		}
	}

	// initialize service config
	config, err := readConfiguration(*configFileName)
	if err != nil {
		log.Fatal(err)
	}

	vPrintf("Database: %s@%s:%d\n", config.Database.Username, config.Database.Host, config.Database.Port)
	vPrintf("Connecting to Database... ")

	// initialize data layer as sql database backend
	err = data.InitLayerAsDB(config.Database)

	if err != nil {
		vPrintf("FAILED\n")
		log.Fatal(err)
	} else {
		vPrintf("success\n")
	}

	// initialize controller layer
	controller.RegisterAll()

	vPrintf("Listening on port %d\n", config.Server.Port)

	// listen and serve
	portStr := fmt.Sprintf(":%d", config.Server.Port)
	err = http.ListenAndServe(portStr, nil)
	if err != nil {
		log.Fatal(err)
	}
}
