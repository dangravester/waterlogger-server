package controller

import (
	"fmt"
	"html/template"
	"net/http"
	"os"
	"strconv"
	"time"

	"bitbucket.org/dangravester/WaterLogger/data"
	"bitbucket.org/dangravester/WaterLogger/model"
	"bitbucket.org/dangravester/WaterLogger/viewmodel"
)

// homeController is the controller for routes "/" and "/home".
type homeController struct {
	tmpl *template.Template
}

func newHomeController() *homeController {
	return &homeController{
		tmpl: template.Must(template.ParseFiles("web/home.html")),
	}
}

// handleFunc is the request handler for homeController.
func (hc *homeController) handleFunc(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		amount, err := strconv.ParseFloat(r.FormValue("amount"), 64)

		// TODO: handle error with prompt
		if err == nil {
			data.InsertLog(model.DrinkEntry{Time: time.Now(), Amount: amount})
		}
	}

	context, err := viewmodel.Home()
	if err != nil {
		fmt.Fprintln(os.Stderr, err) // TODO how to handle errors like this cleanly?
	}

	hc.tmpl.Execute(w, context)
}
