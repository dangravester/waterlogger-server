package controller

import (
	"fmt"
	"net/http"
	"time"

	"bitbucket.org/dangravester/WaterLogger/data"
)

type logsController struct {
}

func (lc *logsController) handleFunc(w http.ResponseWriter, r *http.Request) {
	// NOTE: the idea currently is to print all logs
	start := time.Unix(0, 0)
	finish := time.Now()

	logs, _ := data.LogsInTimeRange(start, finish)
	logsStr := fmt.Sprintf("%v", logs)
	w.Write([]byte(logsStr))
}
