package controller

import "net/http"

// RegisterAll is used to register controllers used by the application.
func RegisterAll() {
	home := newHomeController()
	logs := logsController{}
	http.HandleFunc("/", home.handleFunc)
	http.HandleFunc("/home", home.handleFunc)
	http.HandleFunc("/logs", logs.handleFunc)
}
