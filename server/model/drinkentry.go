package model

import "time"

// DrinkEntry is an entry containing an amount and a time.
type DrinkEntry struct {
	Time   time.Time
	Amount float64
}
