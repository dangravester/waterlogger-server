package data

import (
	"time"

	"bitbucket.org/dangravester/WaterLogger/model"
)

// Layer is an interface representing the model layer of the application.
type Layer interface {
	InsertLog(entry model.DrinkEntry) error
	LogsInTimeRange(start, finish time.Time) ([]model.DrinkEntry, error)
}

var layer Layer

// InitLayer initializes the data layer.
func InitLayer(l Layer) {
	layer = l
}

// InitLayerAsDB initializes the data layer as a DBLayer.
func InitLayerAsDB(config DBConfig) error {
	l, err := NewDBLayer(config)
	InitLayer(l)

	return err
}

// InsertLog inserts a new drink entry into the database.
func InsertLog(entry model.DrinkEntry) error {
	return layer.InsertLog(entry)
}

// LogsInTimeRange returns all drink entry logs within a time range.
func LogsInTimeRange(start, finish time.Time) ([]model.DrinkEntry, error) {
	return layer.LogsInTimeRange(start, finish)
}
