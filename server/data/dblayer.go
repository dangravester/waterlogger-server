package data

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"strings"
	"text/template"
	"time"

	"bitbucket.org/dangravester/WaterLogger/model"
)

const databaseName = "waterlogger_server"

// DBLayer is a data layer utilizing a SQL database connection.
type DBLayer struct {
	db *sql.DB
}

// DBConfig provides a database server configuration
type DBConfig struct {
	Username string `yaml:"username"`
	Passkey  string `yaml:"passkey"`
	Host     string `yaml:"hostname"`
	Port     int    `yaml:"port"`
}

// NewDBLayer constructs a new data layer that uses a SQL database backend.
func NewDBLayer(config DBConfig) (*DBLayer, error) {
	var srv *sql.DB
	var err error

	// connect to database server
	if srv, err = connectToDBServer(config); err != nil {
		return nil, err
	}

	// create database if it doesn't already exist
	if !myDBExists(srv, databaseName) {
		if err = createDB(srv, databaseName); err != nil {
			return nil, err
		}
	}

	var db *sql.DB
	if db, err = connectToDB(config, databaseName); err != nil {
		return nil, err
	}

	// create table if is doesn't already exist
	_, err = db.Exec(dbCreateTableIfDoesNotExistQuery)

	return &DBLayer{db: db}, err
}

// InsertLog adds entry to the database.
func (layer *DBLayer) InsertLog(entry model.DrinkEntry) error {
	_, err := layer.db.Exec(dbInsertLogQuery, entry.Time, entry.Amount)
	return err
}

// LogsInTimeRange returns all drink logs within a given timeframe
func (layer *DBLayer) LogsInTimeRange(start, finish time.Time) ([]model.DrinkEntry, error) {
	entries := []model.DrinkEntry{}

	rows, err := layer.db.Query(dbLogsInTimeRangeQuery, start, finish)

	if err != nil {
		return nil, err
	}

	// accumulate rows into entries
	for rows.Next() {
		var entry model.DrinkEntry
		err = rows.Scan(&entry.Time, &entry.Amount)
		if err != nil {
			return entries, err
		}
		entries = append(entries, entry)
	}

	return entries, nil
}

func connectToDBServer(config DBConfig) (*sql.DB, error) {
	return connectToDB(config, "")
}

func connectToDB(config DBConfig, dbName string) (*sql.DB, error) {
	// get passphrase from key file
	var passphrase string
	if content, err := ioutil.ReadFile(config.Passkey); err == nil {
		passphrase = ":" + string(content)[:len(content)]
	}

	var connectStr string
	if len(dbName) > 0 {
		// apply database name to connection string
		connectStr = fmt.Sprintf("postgres://%s%s@%s:%d/%s?sslmode=disable",
			config.Username, passphrase, config.Host, config.Port, dbName)
	} else {
		// connect to database server without connecting to a particular database
		connectStr = fmt.Sprintf("postgres://%s%s@%s:%d?sslmode=disable", config.Username, passphrase, config.Host, config.Port)
	}

	db, err := sql.Open("postgres", connectStr)

	if err == nil {
		// ensure connection
		err = db.Ping()
	}

	return db, err
}

func createDB(srv *sql.DB, dbName string) error {
	var sb strings.Builder
	tmpl, _ := template.New("").Parse(dbCreateDatabaseQuery)
	tmpl.Execute(&sb, dbName)
	query := sb.String()

	_, err := srv.Exec(query)
	return err
}

func myDBExists(srv *sql.DB, dbName string) bool {
	var sb strings.Builder
	tmpl, _ := template.New("").Parse(dbFindDatabaseByNameQuery)
	tmpl.Execute(&sb, dbName)
	query := sb.String()

	var dbFound bool
	srv.QueryRow(query).Scan(&dbFound)
	return dbFound
}
