package data

const dbFindDatabaseByNameQuery string = `
SELECT EXISTS(
	SELECT datname
	FROM pg_catalog.pg_database
	WHERE lower(datname) = lower('{{.}}')
);`

const dbCreateDatabaseQuery string = `
CREATE DATABASE {{.}};`

const dbCreateTableIfDoesNotExistQuery string = `
CREATE TABLE IF NOT EXISTS drink_logs (
	id SERIAL PRIMARY KEY NOT NULL,
	"time" TIMESTAMP NOT NULL,
	amount REAL NOT NULL
);`

const dbInsertLogQuery string = `
INSERT INTO drink_logs (time, amount)
VALUES ($1, $2)`

const dbLogsInTimeRangeQuery string = `
SELECT time, amount
FROM drink_logs
WHERE time >= $1 AND time <= $2`
