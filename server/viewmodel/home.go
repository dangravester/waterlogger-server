package viewmodel

import (
	"fmt"
	"os"
	"strconv"
	"time"

	"bitbucket.org/dangravester/WaterLogger/data"
)

// HomeView contains all fields needed to construct the home page.
type HomeView struct {
	TotalToday float64
}

// Home returns a HomeView.
func Home() (HomeView, error) {
	// get time range from beginning of today to now
	startToday := time.Now().Truncate(24 * time.Hour)
	now := time.Now()

	// get logs in time range
	logs, err := data.LogsInTimeRange(startToday, now)
	if err != nil {
		fmt.Fprintln(os.Stderr, err) // TODO how to handle errors like this cleanly?
	}

	// get sum of logs in range
	var total float64 = 0.0
	for _, log := range logs {
		total += log.Amount
	}

	// round to a single decimal place
	totalStr := fmt.Sprintf("%.1f", total)
	total, err = strconv.ParseFloat(totalStr, 64)
	if err != nil {
		fmt.Fprintln(os.Stderr, err) // TODO how to handle errors like this cleanly?
	}

	view := HomeView{
		TotalToday: total,
	}
	return view, nil
}
