module bitbucket.org/dangravester/WaterLogger

go 1.13

require (
	github.com/lib/pq v1.2.0
	gopkg.in/yaml.v2 v2.2.4
)
